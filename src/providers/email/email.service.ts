import { Injectable, Logger } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import { ConfigService } from '@nestjs/config';

enum MailDriver {
  log = 'log',
  smtp = 'smtp',
  nodemailerService = 'nodemailerService',
}

enum SMTPEncryption {
  ssl = 'ssl',
}

interface SMTPOptions {
  host: string;
  port: number;
  username: string;
  password: string;
  encryption: SMTPEncryption;
}

interface NodemailerServiceOptions {
  service: string;
  username: string;
  password: string;
}

@Injectable()
export class EmailService {
  private driver: MailDriver =
    MailDriver[this.configService.get('MAIL_DRIVER')] || MailDriver.log;
  private fromAddress = this.configService.get('MAIL_FROM_ADDRESS');
  private fromName = this.configService.get('MAIL_FROM_NAME');

  private smtpOptions: SMTPOptions = {
    host: this.configService.get('MAIL_HOST'),
    port: +this.configService.get('MAIL_PORT'),
    username: this.configService.get('MAIL_USERNAME'),
    password: this.configService.get('MAIL_PASSWORD'),
    encryption:
      SMTPEncryption[this.configService.get('MAIL_SMTP_ENCRYPTION')] ||
      SMTPEncryption.ssl,
  };

  private nodemailerServiceOptions: NodemailerServiceOptions = {
    service: this.configService.get('MAIL_NODEMAILER_SERVICE'),
    username: this.configService.get('MAIL_USERNAME'),
    password: this.configService.get('MAIL_PASSWORD'),
  };

  private transporter: nodemailer.Transporter;

  constructor(private configService: ConfigService) {
    this.setTransporter();
  }

  private setTransporter() {
    switch (this.driver) {
      case MailDriver.log:
        this.transporter = nodemailer.createTransport({
          jsonTransport: true,
        });
        break;
      case MailDriver.nodemailerService:
        this.transporter = nodemailer.createTransport({
          service: this.nodemailerServiceOptions.service,
          auth: {
            user: this.nodemailerServiceOptions.username,
            pass: this.nodemailerServiceOptions.password,
          },
        });
        break;
      case MailDriver.smtp:
        this.transporter = nodemailer.createTransport({
          host: this.smtpOptions.host,
          port: this.smtpOptions.port,
          secure: this.smtpOptions.encryption === SMTPEncryption.ssl,
          auth: {
            user: this.smtpOptions.username,
            pass: this.smtpOptions.password,
          },
        });
        break;
    }

    if (!this.transporter) {
      throw new Error(
        'Mail transporter not set, please make sure your Mail options are set correctly',
      );
    }

    this.transporter.verify();
  }

  sendMail = async (to: string, subject: string, html: string, text?: string) => {
    try {
      const from = `${this.fromName} <${this.fromAddress}>`;
      switch (this.driver) {
        case MailDriver.log:
          const result = await this.transporter.sendMail({
            from,
            to,
            subject,
            text,
            html,
          });
          Logger.log(result);
          break;
        case MailDriver.nodemailerService:
          await this.transporter.sendMail({ from, to, subject, text, html });
          break;
        case MailDriver.smtp:
          await this.transporter.sendMail({ from, to, subject, text, html });
      }
    } catch (error) {
      Logger.error(error, 'EmailService');
    }
  };
}
