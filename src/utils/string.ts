import { randomBytes } from 'crypto';

export const randomStringGenerator = async (): Promise<string> =>
  new Promise((resolve, reject) => {
    randomBytes(100, (err, buf) => {
      if (err) return reject('Random string generation failed');
      return resolve(buf.toString('hex'));
    });
  });
