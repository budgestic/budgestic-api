import { TableColumnOptions } from 'typeorm/schema-builder/options/TableColumnOptions';

interface DatabaseColumns {
  id: TableColumnOptions;
  createdAt: TableColumnOptions;
  updatedAt: TableColumnOptions;
  data: TableColumnOptions;
}

export const DatabaseTypes = {
  mysql: 'mysql',
  sqlite: 'sqlite',
  mariadb: 'mariadb',
};

export const DatabaseColumns: DatabaseColumns = {
  id: {
    name: 'id',
    type: 'int',
    isPrimary: true,
    isGenerated: process.env.DB_TYPE !== DatabaseTypes.sqlite,
    generationStrategy:
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined,
  },
  createdAt: {
    name: 'created_at',
    type: 'datetime',
  },
  updatedAt: {
    name: 'updated_at',
    type: 'datetime',
  },
  data: {
    name: 'data',
    type: process.env.DB_TYPE === DatabaseTypes.sqlite ? 'text' : 'json',
  },
};
