import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreateAccountTable1600005116474 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;
    await queryRunner.createTable(
      new Table({
        name: 'account',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'book_id',
            type: 'int',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'balance',
            type: 'int',
            default: 0,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'account',
      new TableForeignKey({
        columnNames: ['book_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'book',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
