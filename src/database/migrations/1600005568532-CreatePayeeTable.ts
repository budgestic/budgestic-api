import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreatePayeeTable1600005568532 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;
    await queryRunner.createTable(
      new Table({
        name: 'payee',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'book_id',
            type: 'int',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255',
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'payee',
      new TableForeignKey({
        columnNames: ['book_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'book',
        // onDelete: 'SET NULL',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
