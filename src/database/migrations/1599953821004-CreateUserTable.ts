import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreateUserTable1599953821004 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;

    await queryRunner.createTable(
      new Table({
        name: 'user',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'first_name',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'last_name',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'email',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'password',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'active',
            type: 'boolean',
            default: 0,
          },
          {
            name: 'registration_token',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
          {
            name: 'registration_token_expiry_date',
            type: 'datetime',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createIndices('user', [
      new TableIndex({ columnNames: ['email'], isUnique: true }),
      new TableIndex({ columnNames: ['registration_token'], isUnique: true }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
