import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreateBookUserTable1599997451237 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;
    await queryRunner.createTable(
      new Table({
        name: 'book__user',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'user_id',
            type: 'int',
          },
          {
            name: 'book_id',
            type: 'int',
          },
          {
            name: 'is_owner',
            type: 'boolean',
            default: 0,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'book__user',
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'user',
      }),
    );

    await queryRunner.createForeignKey(
      'book__user',
      new TableForeignKey({
        columnNames: ['book_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'book',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
