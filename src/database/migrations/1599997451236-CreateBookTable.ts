import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
const generationStrategy =
  process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;

export class CreateBookTable1599997451236 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'book',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'currency_id',
            type: 'int',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'date_format',
            type: 'enum',
            enum: ['yyyy/mm/dd'],
          },
          {
            name: 'salary_day_chosen',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'salary_type',
            type: 'enum',
            enum: ['weekly', 'bi-weekly', 'semi-monthly', 'monthly'],
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'book',
      new TableForeignKey({
        columnNames: ['currency_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'currency',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
