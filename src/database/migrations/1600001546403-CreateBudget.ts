import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreateBudget1600001546403 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;
    await queryRunner.createTable(
      new Table({
        name: 'budget',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'book_id',
            type: 'int',
          },
          {
            name: 'date_start',
            type: 'date',
          },
          {
            name: 'date_end',
            type: 'date',
          },
          {
            name: 'amount_available',
            type: 'int',
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'budget',
      new TableForeignKey({
        columnNames: ['book_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'book',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
