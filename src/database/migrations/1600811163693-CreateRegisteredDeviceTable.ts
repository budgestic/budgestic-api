import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';
import { DatabaseColumns, DatabaseTypes } from '../options.constant';

export class CreateRegisteredDeviceTable1600811163693 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const type = process.env.DB_TYPE === DatabaseTypes.sqlite ? 'text' : 'json';

    await queryRunner.createTable(
      new Table({
        name: 'registered_device',
        columns: [
          DatabaseColumns.id,
          DatabaseColumns.createdAt,
          DatabaseColumns.updatedAt,
          {
            name: 'user_id',
            type: 'int',
          },
          {
            name: 'refresh_token',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'expiration_date',
            type: 'datetime',
            isNullable: true,
          },
          { ...DatabaseColumns.data, isNullable: true },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'registered_device',
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'user',
      }),
    );

    await queryRunner.createIndices('registered_device', [
      new TableIndex({ columnNames: ['refresh_token'], isUnique: true }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
