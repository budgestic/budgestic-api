import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreateTransactionTable1600006547076 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;

    await queryRunner.createTable(
      new Table({
        name: 'transaction',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'book_id',
            type: 'int',
          },
          {
            name: 'account_id',
            type: 'int',
          },
          {
            name: 'subcategory_id',
            type: 'int',
          },
          {
            name: 'payee_id',
            type: 'int',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP',
          },
          {
            name: 'updated_at',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP',
          },
          {
            name: 'date',
            type: 'date',
          },
          {
            name: 'amount_incoming',
            type: 'int',
            default: 0,
          },
          {
            name: 'amount_outgoing',
            type: 'int',
            default: 0,
          },
          {
            name: 'is_cleared',
            type: 'boolean',
            default: 0,
          },
          {
            name: 'notes',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
          {
            name: 'schedule_type',
            type: 'enum',
            enum: ['daily', 'weekly', 'bi-weekly', 'monthly'],
            isNullable: true,
          },
          {
            name: 'schedule_day_chosen',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'transaction',
      new TableForeignKey({
        columnNames: ['book_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'book',
      }),
    );

    await queryRunner.createForeignKey(
      'transaction',
      new TableForeignKey({
        columnNames: ['account_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'account',
      }),
    );

    await queryRunner.createForeignKey(
      'transaction',
      new TableForeignKey({
        columnNames: ['subcategory_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'subcategory',
      }),
    );

    await queryRunner.createForeignKey(
      'transaction',
      new TableForeignKey({
        columnNames: ['payee_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'payee',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
