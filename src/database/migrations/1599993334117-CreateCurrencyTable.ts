import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreateCurrencyTable1599993334117 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;

    await queryRunner.createTable(
      new Table({
        name: 'currency',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'code',
            type: 'char',
            length: '3',
          },
          {
            name: 'unicode_hex',
            type: 'varchar',
            length: '4',
          },
        ],
      }),
      true,
    );

    await queryRunner.createIndex(
      'currency',
      new TableIndex({ columnNames: ['code'], isUnique: true }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
