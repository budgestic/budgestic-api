import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreateSubCategoryTable1600005897851 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;
    await queryRunner.createTable(
      new Table({
        name: 'subcategory',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'category_id',
            type: 'int',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255',
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'subcategory',
      new TableForeignKey({
        columnNames: ['category_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'category',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
