import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { DatabaseTypes } from '../options.constant';

export class CreateBudgetSubCategoryTable1600006165531
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const isGenerated = process.env.DB_TYPE !== DatabaseTypes.sqlite;
    const generationStrategy =
      process.env.DB_TYPE !== DatabaseTypes.sqlite ? 'increment' : undefined;

    await queryRunner.createTable(
      new Table({
        name: 'budget__subcategory',
        columns: [
          {
            name: 'id',
            type: 'int',
            isGenerated,
            isPrimary: true,
            generationStrategy,
          },
          {
            name: 'budget_id',
            type: 'int',
          },
          {
            name: 'subcategory_id',
            type: 'int',
          },
          {
            name: 'amount',
            type: 'int',
          },
          {
            name: 'amount_available',
            type: 'int',
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'budget__subcategory',
      new TableForeignKey({
        columnNames: ['budget_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'budget',
      }),
    );

    await queryRunner.createForeignKey(
      'budget__subcategory',
      new TableForeignKey({
        columnNames: ['subcategory_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'subcategory',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
