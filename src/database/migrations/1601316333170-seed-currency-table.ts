import { Currency } from 'src/modules/currency/entities/currency.entity';
import { MigrationInterface, QueryRunner, getRepository } from 'typeorm';

export class seedCurrencyTable1601316333170 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await getRepository(Currency).save([
      {
        name: 'British Pounds',
        code: 'GBP',
        unicodeHex: 'a3',
      },
      {
        name: 'Euros',
        code: 'EUR',
        unicodeHex: '20ac',
      },
      {
        name: 'Us Dollars',
        code: 'USD',
        unicodeHex: '0x24',
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
