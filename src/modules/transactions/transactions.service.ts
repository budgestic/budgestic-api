import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { Book } from '../books/entities/book.entity';
import { Subcategory } from '../subcategories/entities/subcategory.entity';
import { User } from '../users/entities/user.entity';
import { Account } from '../accounts/entities/account.entity';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { Transaction } from './entities/transaction.entity';
import { Payee } from '../payees/entities/payee.entity';
import { GetTransactionDto } from './dto/get-transaction-query.dto';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectRepository(Transaction) private transactionRepo: Repository<Transaction>,
    private connection: Connection,
  ) {}

  async create(
    book: Book,
    account: Account,
    subcategory: Subcategory,
    payee: Payee = null,
    createTransactionDto: CreateTransactionDto,
  ) {
    const transaction = new Transaction();
    transaction.book = book;
    transaction.account = account;
    transaction.subcategory = subcategory;
    transaction.payee = payee ? payee : null;
    transaction.date = createTransactionDto.date;
    transaction.amountIncoming = createTransactionDto.amountIncoming;
    transaction.amountOutgoing = createTransactionDto.amountOutgoing;
    transaction.isCleared = createTransactionDto.isCleared;
    transaction.notes = createTransactionDto.notes;
    if (transaction.amountIncoming)
      transaction.account.balance += transaction.amountIncoming;
    else transaction.account.balance -= transaction.amountOutgoing;

    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      await queryRunner.manager.save(transaction);
      await queryRunner.manager.save(transaction.account);
      await queryRunner.commitTransaction();
      queryRunner.rollbackTransaction();
      return transaction;
    } catch (e) {
      await queryRunner.rollbackTransaction();
      throw e;
    } finally {
      await queryRunner.release();
    }
  }

  findAll(
    user: User,
    {
      accountId,
      payeeId,
      categoryId,
      subcategoryId,
      dateFrom,
      dateTo,
      notes,
      isCleared,
      orderByDate,
    }: GetTransactionDto,
  ) {
    const query = this.transactionRepo
      .createQueryBuilder('transaction')
      .leftJoin('transaction.account', 'account')
      .leftJoin('transaction.book', 'book')
      .leftJoin('transaction.payee', 'payee')
      .leftJoin('transaction.subcategory', 'subcategory')
      .leftJoin('subcategory.category', 'category')
      .leftJoin('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .orderBy('transaction.date', orderByDate);

    if (accountId !== null) {
      query.andWhere('account.id = :accountId', {
        accountId,
      });
    }

    if (payeeId !== null) {
      query.andWhere('payee.id =: payeeId', { payeeId });
    }

    if (subcategoryId !== null) {
      query.andWhere('subcategory.id = :subcategoryId', { subcategoryId });
    }

    if (categoryId !== null) {
      query.andWhere('categoryId = :categoryId', {
        categoryId,
      });
    }

    if (dateFrom !== null) {
      query.andWhere('transaction.date >= :dateFrom', { dateFrom });
    }

    if (dateTo !== null) {
      query.andWhere('transaction.date <= :dateTo', { dateTo });
    }

    if (notes !== null) {
      query.andWhere('transaction.notes like :notes', { notes: `%${notes}%` });
    }

    if (isCleared !== null) {
      query.andWhere('transaction.is_cleared = :isCleared', { isCleared });
    }

    return query.getMany();
  }

  findOne(id: number, user: User) {
    return this.transactionRepo
      .createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.book', 'book')
      .leftJoinAndSelect('transaction.account', 'account')
      .leftJoinAndSelect('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .andWhere('transaction.id = :id', { id })
      .getOne();
  }

  async update(
    transaction: Transaction,
    book: Book,
    account: Account,
    subcategory: Subcategory,
    payee: Payee = null,
    updateTransactionDto: UpdateTransactionDto,
  ) {
    transaction.book = book;
    transaction.account = account;
    transaction.subcategory = subcategory;
    transaction.payee = payee ? payee : null;
    transaction.date = updateTransactionDto.date;
    transaction.isCleared = updateTransactionDto.isCleared;
    transaction.notes = updateTransactionDto.notes;

    const newValue = updateTransactionDto.amountIncoming
      ? updateTransactionDto.amountIncoming
      : updateTransactionDto.amountOutgoing * -1;

    const oldValue = transaction.amountIncoming
      ? transaction.amountIncoming
      : transaction.amountOutgoing * -1;

    transaction.account.balance -= oldValue - newValue;
    transaction.amountIncoming = updateTransactionDto.amountIncoming;
    transaction.amountOutgoing = updateTransactionDto.amountOutgoing;

    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      await queryRunner.manager.save(transaction.account);
      await queryRunner.manager.save(transaction);
      await queryRunner.commitTransaction();
    } catch (e) {
      await queryRunner.rollbackTransaction();
      throw e;
    } finally {
      await queryRunner.release();
    }
  }

  async remove(transaction: Transaction) {
    transaction.account.balance += transaction.amountIncoming
      ? -transaction.amountIncoming
      : transaction.amountOutgoing;

    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      await queryRunner.manager.save(transaction.account);
      await queryRunner.manager.remove(transaction);
      await queryRunner.commitTransaction();
      return transaction;
    } catch (e) {
      await queryRunner.rollbackTransaction();
      throw e;
    } finally {
      await queryRunner.release();
    }
  }
}
