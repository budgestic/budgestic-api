import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  NotFoundException,
  BadRequestException,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { BooksService } from '../books/books.service';
import { AccountsService } from '../accounts/accounts.service';
import { PayeesService } from '../payees/payees.service';
import { SubcategoriesService } from '../subcategories/subcategories.service';
import { AuthUser } from '../users/authUser.decorator';
import { User } from '../users/entities/user.entity';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { Transaction } from './entities/transaction.entity';
import { GetTransactionDto } from './dto/get-transaction-query.dto';

@Controller('transactions')
@UseGuards(JwtAuthGuard)
@UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
export class TransactionsController {
  constructor(
    private readonly transactionsService: TransactionsService,
    private readonly bookService: BooksService,
    private readonly accountService: AccountsService,
    private readonly subcategory: SubcategoriesService,
    private readonly payeeService: PayeesService,
  ) {}

  @Post()
  async create(
    @Body() createTransactionDto: CreateTransactionDto,
    @AuthUser() user: User,
  ) {
    const book = await this.bookService.findOne(createTransactionDto.bookId, user);
    if (!book) throw new BadRequestException('Book not found');

    const account = await this.accountService.findOneUsingBook(
      createTransactionDto.accountId,
      book,
    );
    const subcategory = await this.subcategory.findOneUsingBook(
      createTransactionDto.subcategoryId,
      book,
    );
    if (!account) throw new BadRequestException('Account not found');
    if (!subcategory) throw new BadRequestException('Subcategory not found');

    const payee = await this.payeeService.findOneUsingBook(
      createTransactionDto.payeeId,
      book,
    );
    if (createTransactionDto.payeeId && !payee)
      throw new BadRequestException('Payee not found');

    if (!createTransactionDto.amountIncoming && !createTransactionDto.amountOutgoing)
      throw new BadRequestException(
        `You need to set an amount. You can't have a transaction of 0`,
      );

    if (createTransactionDto.amountIncoming && createTransactionDto.amountOutgoing)
      throw new BadRequestException(
        `You can't set both an incoming and outgoing amount`,
      );

    return this.transactionsService.create(
      book,
      account,
      subcategory,
      payee,
      createTransactionDto,
    );
  }

  @Get()
  findAll(@Query() getTransactionDto: GetTransactionDto, @AuthUser() user: User) {
    return this.transactionsService.findAll(user, getTransactionDto);
  }

  @Get(':id')
  async findOne(@Param('id') id: number, @AuthUser() user: User) {
    const transaction = await this.transactionsService.findOne(id, user);
    if (!transaction) throw new NotFoundException('Transaction not found');
    return transaction;
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() updateTransactionDto: UpdateTransactionDto,
    @AuthUser() user: User,
  ) {
    const transaction = await this.transactionsService.findOne(id, user);
    if (!transaction) throw new NotFoundException('Transaction not found');

    const book = await this.bookService.findOne(updateTransactionDto.bookId, user);
    if (!book) throw new BadRequestException('Book not found');

    const account = await this.accountService.findOneUsingBook(
      updateTransactionDto.accountId,
      book,
    );
    const subcategory = await this.subcategory.findOneUsingBook(
      updateTransactionDto.subcategoryId,
      book,
    );
    if (!account) throw new BadRequestException('Account not found');
    if (!subcategory) throw new BadRequestException('Subcategory not found');

    const payee = await this.payeeService.findOneUsingBook(
      updateTransactionDto.payeeId,
      book,
    );
    if (updateTransactionDto.payeeId && !payee)
      throw new BadRequestException('Payee not found');

    if (!updateTransactionDto.amountIncoming && !updateTransactionDto.amountOutgoing)
      throw new BadRequestException(
        `You need to set an amount. You can't have a transaction of 0`,
      );

    if (updateTransactionDto.amountIncoming && updateTransactionDto.amountOutgoing)
      throw new BadRequestException(
        `You can't set both an incoming and outgoing amount`,
      );

    return this.transactionsService.update(
      transaction,
      book,
      account,
      subcategory,
      payee,
      updateTransactionDto,
    );
  }

  @Delete(':id')
  async remove(@Param('id') id: number, @AuthUser() user: User) {
    const transaction = await this.transactionsService.findOne(id, user);
    if (!transaction) throw new NotFoundException('Transaction not found');
    await this.transactionsService.remove(transaction);
    return {};
  }
}
