import { ApiProperty } from '@nestjs/swagger';
import { Account } from 'src/modules/accounts/entities/account.entity';
import { Book } from 'src/modules/books/entities/book.entity';
import { Payee } from 'src/modules/payees/entities/payee.entity';
import { Subcategory } from 'src/modules/subcategories/entities/subcategory.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Transaction {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn({ name: 'updated_at', type: 'datetime' })
  updatedAt: Date;

  @ApiProperty()
  @Column({ type: 'date' })
  date: Date;

  @ApiProperty()
  @Column({ name: 'amount_incoming' })
  amountIncoming: number;

  @ApiProperty()
  @Column({ name: 'amount_outgoing' })
  amountOutgoing: number;

  @ApiProperty()
  @Column({ name: 'is_cleared' })
  isCleared: boolean;

  @ApiProperty()
  @Column()
  notes: string;

  @ManyToOne(
    type => Book,
    book => book.transactions,
  )
  @JoinColumn({ name: 'book_id' })
  book: Book;

  @ManyToOne(
    type => Account,
    account => account.transactions,
  )
  @JoinColumn({ name: 'account_id' })
  account: Account;

  @ManyToOne(
    type => Subcategory,
    subcategory => subcategory.transactions,
  )
  @JoinColumn({ name: 'subcategory_id' })
  subcategory: Subcategory;

  @ManyToOne(
    type => Payee,
    payee => payee.transactions,
  )
  @JoinColumn({ name: 'payee_id' })
  payee: Payee;
}
