import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { Transaction } from './entities/transaction.entity';
import { AccountsModule } from '../accounts/accounts.module';
import { SubcategoriesModule } from '../subcategories/subcategories.module';
import { PayeesModule } from '../payees/payees.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BooksModule } from '../books/books.module';

@Module({
  controllers: [TransactionsController],
  providers: [TransactionsService],
  imports: [
    TypeOrmModule.forFeature([Transaction]),
    BooksModule,
    AccountsModule,
    SubcategoriesModule,
    PayeesModule,
  ],
})
export class TransactionsModule {}
