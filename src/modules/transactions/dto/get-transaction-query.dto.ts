import { Transform, Type } from 'class-transformer';
import { IsBoolean, IsDate, IsIn, IsNotEmpty, IsOptional } from 'class-validator';
import { Account } from 'src/modules/accounts/entities/account.entity';
import { Category } from 'src/modules/categories/entities/category.entity';
import { Payee } from 'src/modules/payees/entities/payee.entity';
import { Subcategory } from 'src/modules/subcategories/entities/subcategory.entity';
import { Transaction } from '../entities/transaction.entity';

export class GetTransactionDto {
  @IsOptional()
  @IsNotEmpty()
  accountId?: Account['id'] = null;

  @IsOptional()
  payeeId?: Payee['id'] = null;

  @IsOptional()
  categoryId?: Category['id'] = null;

  @IsOptional()
  subcategoryId?: Subcategory['id'] = null;

  @IsOptional()
  @Type(() => Date)
  @IsDate()
  dateFrom?: Date = null;

  @IsOptional()
  @Type(() => Date)
  @IsDate()
  dateTo?: Date = null;

  @IsOptional()
  notes?: Transaction['notes'] = null;

  @IsOptional()
  @Transform(({ value }) => {
    if (value === 'true') return true;
    if (value === 'false') return false;
    return value;
  })
  @IsBoolean()
  isCleared?: Transaction['isCleared'] = null;

  @IsIn(['ASC', 'DESC'])
  @IsOptional()
  orderByDate: 'ASC' | 'DESC' = 'ASC';
}
