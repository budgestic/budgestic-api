import { Type } from 'class-transformer';
import { IsDate, IsNotEmpty, IsOptional, MaxLength, Min } from 'class-validator';

export class CreateTransactionDto {
  @IsNotEmpty()
  bookId: number;

  @IsNotEmpty()
  accountId: number;

  @IsNotEmpty()
  subcategoryId: number;

  @IsNotEmpty()
  @IsOptional()
  payeeId?: number;

  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  date: Date;

  @IsNotEmpty()
  @Min(0)
  amountIncoming: number;

  @IsNotEmpty()
  @Min(0)
  amountOutgoing: number;

  @IsNotEmpty()
  isCleared: boolean;

  @IsNotEmpty()
  @IsOptional()
  @MaxLength(255)
  notes?: string;
}
