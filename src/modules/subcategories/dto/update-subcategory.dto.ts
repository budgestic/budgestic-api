import { PartialType } from '@nestjs/mapped-types';
import { CreateSubcategoryDto } from './create-subcategory.dto';

export class UpdateSubCategoryDto extends CreateSubcategoryDto {}
