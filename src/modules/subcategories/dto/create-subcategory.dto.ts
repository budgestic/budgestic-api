import { IsNotEmpty } from 'class-validator';

export class CreateSubcategoryDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  categoryId: number;
}
