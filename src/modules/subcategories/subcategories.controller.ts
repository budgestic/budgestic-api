import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
  Query,
  NotFoundException,
} from '@nestjs/common';
import { SubcategoriesService } from './subcategories.service';
import { CreateSubcategoryDto } from './dto/create-subcategory.dto';
import { UpdateSubCategoryDto } from './dto/update-subcategory.dto';
import { Subcategory } from './entities/subcategory.entity';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { CategoryInterceptor } from 'src/interceptors/category.interceptor';
import { Category } from '../categories/entities/category.entity';
import { UserCategory } from '../categories/category.decorator';
import { CategoriesService } from '../categories/categories.service';
import { AuthUser } from '../users/authUser.decorator';
import { User } from '../users/entities/user.entity';

@Controller('subcategories')
@UseGuards(JwtAuthGuard)
@UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
export class SubcategoriesController {
  constructor(
    private readonly subCategoriesService: SubcategoriesService,
    private readonly categoryService: CategoriesService,
  ) {}

  @Post()
  @UseInterceptors(CategoryInterceptor)
  create(@Body() body: CreateSubcategoryDto, @UserCategory() category: Category) {
    const subCategory = new Subcategory();
    subCategory.name = body.name;
    subCategory.category = category;
    return this.subCategoriesService.create(subCategory);
  }

  @Get()
  async findAll(@Query('categoryId') categoryId: number, @AuthUser() user: User) {
    if (!categoryId) {
      return this.subCategoriesService.findAll(user);
    }

    const category = await this.categoryService.findOne(categoryId, user);
    if (!category) throw new NotFoundException('Category not found');

    return this.subCategoriesService.findAllUsingCategory(category);
  }

  @Get(':id')
  async findOne(@Param('id') id: number, @AuthUser() user: User) {
    const subCategory = await this.subCategoriesService.findOne(id, user);
    if (!subCategory) throw new NotFoundException('Subcategory not found');
    return subCategory;
  }

  @Put(':id')
  @UseInterceptors(CategoryInterceptor)
  async update(
    @Param('id') id: number,
    @Body() body: UpdateSubCategoryDto,
    @AuthUser() user: User,
    @UserCategory() category: Category,
  ) {
    const subCategory = await this.subCategoriesService.findOne(id, user);
    if (!subCategory) throw new NotFoundException('Subcategory not found');

    subCategory.name = body.name;
    subCategory.category = category;
    return this.subCategoriesService.update(subCategory);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.subCategoriesService.remove(+id);
  }
}
