import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm/repository/Repository';
import { Book } from '../books/entities/book.entity';
import { Category } from '../categories/entities/category.entity';
import { User } from '../users/entities/user.entity';
import { CreateSubcategoryDto } from './dto/create-subcategory.dto';
import { UpdateSubCategoryDto } from './dto/update-subcategory.dto';
import { Subcategory } from './entities/subcategory.entity';

@Injectable()
export class SubcategoriesService {
  constructor(
    @InjectRepository(Subcategory) private subCategoryRepo: Repository<Subcategory>,
  ) {}

  create(subCategory: Subcategory) {
    return this.subCategoryRepo.save(subCategory);
  }

  findAll(user: User) {
    return this.subCategoryRepo
      .createQueryBuilder('subcategory')
      .leftJoinAndSelect('subcategory.category', 'category')
      .leftJoinAndSelect('category.book', 'book')
      .leftJoin('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .getMany();
  }

  findAllUsingCategory(category: Category) {
    return this.subCategoryRepo
      .createQueryBuilder('subcategory')
      .leftJoinAndSelect('subcategory.category', 'category')
      .leftJoinAndSelect('category.book', 'book')
      .where('category.id = :categoryId', { categoryId: category.id })
      .getMany();
  }

  findOne(id: Subcategory['id'], user) {
    return this.subCategoryRepo
      .createQueryBuilder('subcategory')
      .leftJoinAndSelect('subcategory.category', 'category')
      .leftJoinAndSelect('category.book', 'book')
      .leftJoin('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .andWhere('subcategory.id = :id', { id })
      .getOne();
  }

  findOneUsingBook(id: Subcategory['id'], book: Book) {
    return this.subCategoryRepo
      .createQueryBuilder('subcategory')
      .leftJoinAndSelect('subcategory.category', 'category')
      .leftJoinAndSelect('category.book', 'book')
      .where('book.id = :bookId', { bookId: book.id })
      .andWhere('subcategory.id = :id', { id })
      .getOne();
  }

  update(subcategory: Subcategory) {
    return this.subCategoryRepo.save(subcategory);
  }

  remove(id: number) {
    return `This action removes a #${id} subCategory`;
  }
}
