import { ApiProperty } from '@nestjs/swagger';
import { Category } from 'src/modules/categories/entities/category.entity';
import { Transaction } from 'src/modules/transactions/entities/transaction.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Subcategory {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  name: string;

  @ManyToOne(
    type => Category,
    category => category.subcategories,
  )
  @JoinColumn({ name: 'category_id' })
  category: Category;

  @OneToMany(
    type => Transaction,
    transaction => transaction.subcategory,
  )
  transactions!: Transaction[];
}
