import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import * as dayjs from 'dayjs';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import AuthRegisterRequestDto from '../auth/dto/auth-register-request.dto';
import { RegisteredDevice } from '../registered-devices/registered-device.entity';
import { randomStringGenerator } from 'src/utils/string';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  generateHashFromPassword = async (password: string): Promise<string> => {
    const saltRounds = process.env.BCRYPT_SALT_ROUNDS || 13;
    return bcrypt.hash(password, saltRounds);
  };

  create = async (data: AuthRegisterRequestDto): Promise<User> => {
    const user = new User();
    user.firstName = data.firstName;
    user.lastName = data.lastName;
    user.email = data.email;
    user.password = await this.generateHashFromPassword(data.password);
    user.active = false;
    user.registrationToken = await randomStringGenerator();
    user.registrationTokenExpiryDate = dayjs()
      .add(1, 'day')
      .toDate();
    return this.userRepo.save(user);
  };

  update = async (user: User): Promise<User> => {
    return this.userRepo.save(user);
  };

  findUsingId = async (id: number): Promise<User> => {
    return this.userRepo.findOne(id);
  };

  findUsingRegistrationToken = async (registrationToken: string): Promise<User> => {
    return this.userRepo.findOne({ where: { registrationToken } });
  };

  findUsingEmail = async (email: string): Promise<User> => {
    return this.userRepo.findOne({ where: { email } });
  };

  doesEmailAlreadyExist = async (email: string): Promise<boolean> => {
    const count = await this.userRepo.count({ where: { email } });
    return !!count;
  };

  validateUser = async (user: User, password: string): Promise<boolean> => {
    return bcrypt.compare(password, user.password);
  };
}
