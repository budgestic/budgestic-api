import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { RegisteredDevice } from '../../registered-devices/registered-device.entity';
import { BookToUser } from '../../books/entities/book-to-user.entity';

@Entity()
export class User {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ name: 'first_name' })
  firstName: string;

  @ApiProperty()
  @Column({ name: 'last_name' })
  lastName: string;

  @ApiProperty()
  @Column()
  email: string;

  @ApiProperty()
  @Column()
  @Exclude()
  password: string;

  @ApiProperty()
  @Column()
  active: boolean;

  @ApiProperty()
  @Column({ name: 'registration_token' })
  @Exclude()
  registrationToken: string;

  @ApiProperty()
  @Column({ name: 'registration_token_expiry_date', type: 'datetime' })
  @Exclude()
  registrationTokenExpiryDate: Date;

  @OneToMany(
    type => RegisteredDevice,
    registeredDevice => registeredDevice.user,
  )
  registeredDevices: RegisteredDevice[];

  @OneToMany(
    type => BookToUser,
    bookToUser => bookToUser.user,
  )
  bookToUsers!: BookToUser[];
}
