import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  UsePipes,
  ValidationPipe,
  UseInterceptors,
  Query,
  NotFoundException,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { BookInterceptor } from 'src/interceptors/book.interceptor';
import { UserBook } from '../books/book.decorator';
import { BooksService } from '../books/books.service';
import { AuthUser } from '../users/authUser.decorator';
import { User } from '../users/entities/user.entity';
import { AccountsService } from './accounts.service';
import { CreateAccountDto } from './dto/create-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { Account } from './entities/account.entity';

@Controller('accounts')
@UseGuards(JwtAuthGuard)
@UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
export class AccountsController {
  constructor(
    private readonly accountsService: AccountsService,
    private readonly booksService: BooksService,
  ) {}

  @Post()
  @UseInterceptors(BookInterceptor)
  create(@Body() body: CreateAccountDto, @UserBook() book) {
    const account = new Account();
    account.name = body.name;
    account.balance = body.balance;
    account.book = book;
    return this.accountsService.create(account);
  }

  @Get()
  @UseInterceptors(BookInterceptor)
  async findAll(@Query('bookId') bookId: number, @AuthUser() user: User) {
    if (!bookId) {
      return this.accountsService.findAll(user);
    }

    const book = await this.booksService.findOne(bookId, user);
    if (!book) throw new NotFoundException('Book not found');
    return this.accountsService.findAllUsingBook(book);
  }

  @Get(':id')
  async findOne(@Param('id') id: number, @AuthUser() user: User) {
    const account = await this.accountsService.findOne(id, user);
    if (!account) throw new NotFoundException('Account not found');
    return account;
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() body: UpdateAccountDto,
    @AuthUser() user: User,
  ) {
    const account = await this.accountsService.findOne(id, user);
    if (!account) throw new NotFoundException('Account not found');

    account.name = body.name;
    account.balance = body.balance;
    return this.accountsService.update(account);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.accountsService.remove(+id);
  }
}
