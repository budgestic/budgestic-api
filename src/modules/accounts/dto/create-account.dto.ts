import { IsInt, IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

export class CreateAccountDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsNumber({ maxDecimalPlaces: 0 })
  balance: number;

  @IsNotEmpty()
  @IsInt()
  @IsPositive()
  bookId: number;
}
