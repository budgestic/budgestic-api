import { Module } from '@nestjs/common';
import { AccountsService } from './accounts.service';
import { AccountsController } from './accounts.controller';
import { Account } from './entities/account.entity';
import { BooksModule } from '../books/books.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [AccountsController],
  providers: [AccountsService],
  imports: [TypeOrmModule.forFeature([Account]), BooksModule],
  exports: [AccountsService],
})
export class AccountsModule {}
