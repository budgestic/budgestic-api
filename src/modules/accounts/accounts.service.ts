import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from '../books/entities/book.entity';
import { User } from '../users/entities/user.entity';
import { UpdateAccountDto } from './dto/update-account.dto';
import { Account } from './entities/account.entity';

@Injectable()
export class AccountsService {
  constructor(@InjectRepository(Account) private accountRepo: Repository<Account>) {}

  create(account: Account) {
    return this.accountRepo.save(account);
  }

  findAll(user: User) {
    return this.accountRepo
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.book', 'book')
      .leftJoin('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .getMany();
  }

  findAllUsingBook(book: Book) {
    return this.accountRepo
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.book', 'book')
      .where('book.id = :bookId', { bookId: book.id })
      .getMany();
  }

  findOne(id: Account['id'], user: User) {
    return this.accountRepo
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.book', 'book')
      .leftJoinAndSelect('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .andWhere('account.id = :id', { id })
      .getOne();
  }

  findOneUsingBook(id: Account['id'], book: Book) {
    return this.accountRepo
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.book', 'book')
      .where('book.id = :bookId', { bookId: book.id })
      .andWhere('account.id = :id', { id })
      .getOne();
  }

  update(account: Account) {
    return this.accountRepo.save(account);
  }

  remove(id: number) {
    return `This action removes a #${id} account`;
  }
}
