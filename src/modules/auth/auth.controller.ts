import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
  Res,
  Req,
  HttpCode,
  Patch,
} from '@nestjs/common';
import * as dayjs from 'dayjs';
import AuthLoginRequestDto from './dto/auth-login-request.dto';
import AuthRegisterRequestDto from './dto/auth-register-request.dto';
import { UsersService } from '../users/users.service';
import { User } from '../users/entities/user.entity';
import { EmailService } from 'src/providers/email/email.service';
import AuthPasswordResetRequestDto from './dto/auth-password-reset-request.dto';
import { AuthService } from './auth.service';
import { Request, Response } from 'express';
import { randomStringGenerator } from '../../utils/string';
import { RegisteredDevicesService } from '../registered-devices/registered-devices.service';
import AuthPasswordResetVerificationRequestDto from './dto/auth-password-reset-verification-request.dto';

@Controller('auth')
@UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
export class AuthController {
  constructor(
    private userService: UsersService,
    private emailService: EmailService,
    private authService: AuthService,
    private registeredDeviceService: RegisteredDevicesService,
  ) {}

  @Post('login')
  async login(@Body() body: AuthLoginRequestDto, @Res() res: Response) {
    const user = await this.userService.findUsingEmail(body.email);
    if (!user) throw new UnauthorizedException('Email/password incorrect');

    const validationPassed = await this.userService.validateUser(
      user,
      body.password,
    );

    if (!validationPassed)
      throw new UnauthorizedException('Email/password incorrect');

    if (!user.active)
      throw new UnauthorizedException('You need to verify your account');

    const accessToken = this.authService.generateAccessToken(user);
    const refreshToken = await randomStringGenerator();
    await this.registeredDeviceService.create(user, refreshToken);

    res.cookie('refreshToken', refreshToken, { httpOnly: true });
    res.json({ accessToken, refreshToken });
  }

  @Post('refresh')
  async refresh(
    @Body() body: { refreshToken?: string },
    @Req() req: any,
    @Res() res: Response,
  ) {
    const refreshToken = body.refreshToken
      ? body.refreshToken
      : req.cookies.refreshToken;

    const registeredDevice = await this.registeredDeviceService.findUsingToken(
      refreshToken,
    );
    if (!registeredDevice)
      throw new UnauthorizedException('Refresh token is invalid');
    const { user } = registeredDevice;
    const accessToken = this.authService.generateAccessToken(user);
    const newRefreshToken = await randomStringGenerator();
    registeredDevice.refreshToken = newRefreshToken;
    await this.registeredDeviceService.update(registeredDevice);
    res.cookie('refreshToken', newRefreshToken, { httpOnly: true });
    res.json({ accessToken, newRefreshToken });
  }

  @Post('register')
  async register(@Body() body: AuthRegisterRequestDto): Promise<User> {
    const doesEmailExist = await this.userService.doesEmailAlreadyExist(body.email);
    if (doesEmailExist) throw new BadRequestException('Email already exists');

    const user = await this.userService.create(body);
    this.emailService.sendMail(
      user.email,
      'Welcome to budgestic',
      `Click here to verify: http://localhost:3100/auth/verifyEmail/${user.registrationToken}`,
      '',
    );
    return user;
  }

  @Get('verifyEmail/:registrationToken')
  async verifyEmail(@Param('registrationToken') registrationToken: string) {
    let user = await this.userService.findUsingRegistrationToken(registrationToken);
    if (!user) throw new NotFoundException('User not found');

    const hasRegistrationTokenExpired = dayjs().isAfter(
      user.registrationTokenExpiryDate,
    );
    if (hasRegistrationTokenExpired)
      throw new UnauthorizedException(
        'Token has expired, please reset your password and try again',
      );
    user.active = true;
    user.registrationToken = null;
    user.registrationTokenExpiryDate = null;
    await this.userService.update(user);

    return 'Email verified';
  }

  @Post('passwordReset')
  @HttpCode(200)
  async passwordReset(@Body() body: AuthPasswordResetRequestDto) {
    const user = await this.userService.findUsingEmail(body.email);
    if (!user) throw new NotFoundException('Email not found');

    user.registrationToken = await randomStringGenerator();
    user.registrationTokenExpiryDate = dayjs()
      .add(1, 'day')
      .toDate();
    await this.userService.update(user);
    this.emailService.sendMail(
      user.email,
      'Password reset budgestic',
      `Click here to verify: http://localhost:3100/auth/passwordVerification/${user.registrationToken}`,
      '',
    );
    return 'Password reset';
  }

  @Patch('passwordVerification/:registrationToken')
  async passwordVerification(
    @Body() body: AuthPasswordResetVerificationRequestDto,
    @Param('registrationToken') registrationToken: string,
  ) {
    let user = await this.userService.findUsingRegistrationToken(registrationToken);

    if (!user) throw new NotFoundException('User not found');
    if (body.password !== body.password2)
      throw new BadRequestException('Passwords do not match!');

    user.password = await this.userService.generateHashFromPassword(body.password);
    user.active = true;
    user.registrationToken = null;
    await this.userService.update(user);
    return 'Password successfully reset';
  }
}
