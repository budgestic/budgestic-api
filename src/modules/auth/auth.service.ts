import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { User } from '../users/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  generateAccessToken = (user: User): string => {
    const payload = { sub: plainToClass(User, user) };
    return this.jwtService.sign(payload);
  };
}
