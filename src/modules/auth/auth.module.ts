import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from '../users/users.module';
import { EmailModule } from 'src/providers/email/email.module';
import { JwtStrategy } from './jwt.strategy';
import { RegisteredDevicesModule } from '../registered-devices/registered-devices.module';

@Module({
  imports: [
    JwtModule.registerAsync({
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('AUTH_JWT_SECRET'),
        signOptions: { expiresIn: configService.get('AUTH_JWT_EXPIRATION') },
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    EmailModule,
    RegisteredDevicesModule,
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
