import { IsEmail, IsString } from 'class-validator';

export default class AuthRegisterRequestDto {
  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsEmail()
  email: string;

  @IsString()
  password: string;
}
