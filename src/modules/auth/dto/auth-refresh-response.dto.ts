export default class AuthRefreshResponseDto {
  accessToken: string;
  refreshToken: string;
}
