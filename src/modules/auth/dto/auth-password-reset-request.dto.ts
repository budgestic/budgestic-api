import { IsEmail, IsString } from 'class-validator';

export default class AuthPasswordResetRequestDto {
  @IsEmail()
  email: string;
}
