import { IsEmail, IsString } from 'class-validator';
export default class AuthLoginRequestDto {
  @IsEmail()
  email: string;

  @IsString()
  password: string;
}
