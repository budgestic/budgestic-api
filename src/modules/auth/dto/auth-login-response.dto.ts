import { User } from 'src/modules/users/entities/user.entity';

export default class AuthLoginResponseDto {
  user: User;
  accessToken: string;
}
