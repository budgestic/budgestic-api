import { IsString } from 'class-validator';

export default class AuthPasswordResetVerificationRequestDto {
  @IsString()
  password: string;

  @IsString()
  password2: string;
}
