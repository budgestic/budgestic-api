import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from '../books/entities/book.entity';
import { User } from '../users/entities/user.entity';
import { Category } from './entities/category.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category) private categoryRepo: Repository<Category>,
  ) {}

  create(category: Category) {
    return this.categoryRepo.save(category);
  }

  findAllUsingBook(book: Book) {
    return this.categoryRepo.find({ where: { book } });
  }

  findOneUsingBook(id: Category['id'], book: Book) {
    return this.categoryRepo.findOne({ where: { id, book } });
  }

  findOne(id: Category['id'], user: User) {
    return this.categoryRepo
      .createQueryBuilder('category')
      .leftJoinAndSelect('category.book', 'book')
      .leftJoinAndSelect('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .andWhere('category.id = :id', { id })
      .getOne();
  }

  update(category: Category) {
    return this.categoryRepo.save(category);
  }

  remove(category: Category) {
    return this.categoryRepo.remove(category);
  }
}
