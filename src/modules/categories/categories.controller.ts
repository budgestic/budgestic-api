import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  UsePipes,
  ValidationPipe,
  UseInterceptors,
  NotFoundException,
} from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { BookInterceptor } from '../../interceptors/book.interceptor';
import { UserBook } from '../books/book.decorator';
import { Book } from '../books/entities/book.entity';
import { Category } from './entities/category.entity';
import { AuthUser } from '../users/authUser.decorator';
import { User } from '../users/entities/user.entity';

@Controller('categories')
@UseGuards(JwtAuthGuard)
@UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Post()
  @UseInterceptors(BookInterceptor)
  async create(
    @Body() createCategoryDto: CreateCategoryDto,
    @UserBook() book: Book,
  ) {
    let category = new Category();
    category.name = createCategoryDto.name;
    category.book = book;
    category = await this.categoriesService.create(category);
    return { category };
  }

  @Get()
  @UseInterceptors(BookInterceptor)
  async findAll(@UserBook() book: Book) {
    const categories = await this.categoriesService.findAllUsingBook(book);
    return { categories };
  }

  @Get(':id')
  async findOne(@Param('id') id: number, @AuthUser() user: User) {
    const category = await this.categoriesService.findOne(+id, user);
    if (!category) throw new NotFoundException('Category not found');
    return { category };
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() updateCategoryDto: UpdateCategoryDto,
    @AuthUser() user: User,
  ) {
    const category = await this.categoriesService.findOne(+id, user);
    if (!category) throw new NotFoundException('Category not found');
    category.name = updateCategoryDto.name;
    this.categoriesService.update(category);
    return { category };
  }

  @Delete(':id')
  async remove(@Param('id') id: number, @AuthUser() user: User) {
    const category = await this.categoriesService.findOne(+id, user);
    if (!category) throw new NotFoundException('Category not found');
    await this.categoriesService.remove(category);
    return 'Category removed!';
  }
}
