import { ApiProperty } from '@nestjs/swagger';
import { Book } from '../../books/entities/book.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Subcategory } from 'src/modules/subcategories/entities/subcategory.entity';

@Entity()
export class Category {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  name: string;

  @ManyToOne(
    type => Book,
    book => book.categories,
  )
  @JoinColumn({ name: 'book_id' })
  book: Book;

  @OneToMany(
    type => Subcategory,
    subCategory => subCategory.category,
  )
  subcategories!: Subcategory[];
}
