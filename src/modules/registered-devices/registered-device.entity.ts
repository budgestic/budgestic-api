import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { User } from '../users/entities/user.entity';

interface Data {
  deviceName?: string;
  token?: string;
  ipAddress?: string;
}

@Entity()
export class RegisteredDevice {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @CreateDateColumn()
  @Column({ name: 'created_at' })
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn()
  @Column({ name: 'updated_at' })
  updatedAt: Date;

  @ApiProperty()
  @Column({ name: 'refresh_token', unique: true })
  refreshToken: string;

  @ApiProperty()
  @Column({ name: 'expiration_date' })
  expirationDate: string;

  @ApiProperty()
  @Column({ type: 'simple-json' })
  @Exclude()
  data: Data;

  @ManyToOne(
    type => User,
    user => user.registeredDevices,
  )
  @JoinColumn({ name: 'user_id' })
  user: User;

  @BeforeInsert()
  setInsertDates() {
    this.createdAt = new Date();
    this.updatedAt = new Date();
  }

  @BeforeUpdate()
  setUpdateDates() {
    this.updatedAt = new Date();
  }
}
