import { Test, TestingModule } from '@nestjs/testing';
import { RegisteredDevicesService } from './registered-devices.service';

describe('RegisteredDevicesService', () => {
  let service: RegisteredDevicesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RegisteredDevicesService],
    }).compile();

    service = module.get<RegisteredDevicesService>(RegisteredDevicesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
