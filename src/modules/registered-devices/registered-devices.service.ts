import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import dayjs from 'dayjs';
import { async } from 'rxjs';
import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { RegisteredDevice } from './registered-device.entity';

@Injectable()
export class RegisteredDevicesService {
  constructor(
    @InjectRepository(RegisteredDevice)
    private registeredDeviceRepo: Repository<RegisteredDevice>,
  ) {}

  create = async (user: User, refreshToken: string): Promise<RegisteredDevice> => {
    const registeredDevice = new RegisteredDevice();
    registeredDevice.refreshToken = refreshToken;
    registeredDevice.user = user;
    return await this.registeredDeviceRepo.save(registeredDevice);
  };

  findUsingToken = async (refreshToken: string) => {
    return await this.registeredDeviceRepo.findOne({
      where: { refreshToken },
      relations: ['user'],
    });
  };

  update = async (registeredDevice: RegisteredDevice) => {
    return this.registeredDeviceRepo.save(registeredDevice);
  };
}
