import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegisteredDevice } from './registered-device.entity';
import { RegisteredDevicesService } from './registered-devices.service';

@Module({
  imports: [TypeOrmModule.forFeature([RegisteredDevice])],
  providers: [RegisteredDevicesService],
  exports: [RegisteredDevicesService],
})
export class RegisteredDevicesModule {}
