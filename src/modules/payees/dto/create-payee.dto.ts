import { IsNotEmpty, IsInt, IsPositive } from 'class-validator';

export class CreatePayeeDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsInt()
  @IsPositive()
  bookId: number;
}
