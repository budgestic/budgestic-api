import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseInterceptors,
  Query,
  NotFoundException,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { PayeesService } from './payees.service';
import { CreatePayeeDto } from './dto/create-payee.dto';
import { UpdatePayeeDto } from './dto/update-payee.dto';
import { BookInterceptor } from 'src/interceptors/book.interceptor';
import { Payee } from './entities/payee.entity';
import { UserBook } from '../books/book.decorator';
import { AuthUser } from '../users/authUser.decorator';
import { User } from '../users/entities/user.entity';
import { BooksService } from '../books/books.service';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';

@Controller('payees')
@UseGuards(JwtAuthGuard)
@UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
export class PayeesController {
  constructor(
    private readonly payeesService: PayeesService,
    private readonly booksService: BooksService,
  ) {}

  @Post()
  @UseInterceptors(BookInterceptor)
  create(@Body() body: CreatePayeeDto, @UserBook() book) {
    const payee = new Payee();
    payee.name = body.name;
    payee.book = book;
    return this.payeesService.create(payee);
  }

  @Get()
  @UseInterceptors(BookInterceptor)
  async findAll(@Query('bookId') bookId: number, @AuthUser() user: User) {
    const book = await this.booksService.findOne(bookId, user);
    if (!book) throw new NotFoundException('Book not found');
    return this.payeesService.findAllUsingBook(book);
  }

  @Get(':id')
  async findOne(@Param('id') id: number, @AuthUser() user: User) {
    const payee = await this.payeesService.findOne(id, user);
    if (!payee) throw new NotFoundException('Payee not found');
    return payee;
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() body: UpdatePayeeDto,
    @AuthUser() user: User,
  ) {
    const payee = await this.payeesService.findOne(id, user);
    if (!payee) throw new NotFoundException('Payee not found');

    payee.name = body.name;
    return this.payeesService.update(payee);
  }

  @Delete(':id')
  async remove(@Param('id') id: number, @AuthUser() user: User) {
    const payee = await this.payeesService.findOne(id, user);
    if (!payee) throw new NotFoundException('Payee not found');
    return this.payeesService.remove(payee);
  }
}
