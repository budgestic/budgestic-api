import { Test, TestingModule } from '@nestjs/testing';
import { PayeesController } from './payees.controller';
import { PayeesService } from './payees.service';

describe('PayeesController', () => {
  let controller: PayeesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PayeesController],
      providers: [PayeesService],
    }).compile();

    controller = module.get<PayeesController>(PayeesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
