import { Module } from '@nestjs/common';
import { PayeesService } from './payees.service';
import { PayeesController } from './payees.controller';
import { Payee } from './entities/payee.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BooksModule } from '../books/books.module';

@Module({
  controllers: [PayeesController],
  providers: [PayeesService],
  imports: [TypeOrmModule.forFeature([Payee]), BooksModule],
  exports: [PayeesService],
})
export class PayeesModule {}
