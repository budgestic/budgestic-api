import { ApiProperty } from '@nestjs/swagger';
import { Book } from 'src/modules/books/entities/book.entity';
import { Transaction } from 'src/modules/transactions/entities/transaction.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Payee {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  name: string;

  @ManyToOne(
    type => Book,
    book => book.payees,
  )
  @JoinColumn({ name: 'book_id' })
  book: Book;

  @OneToMany(
    type => Transaction,
    transaction => transaction.payee,
  )
  transactions!: Transaction[];
}
