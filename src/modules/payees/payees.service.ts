import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from '../books/entities/book.entity';
import { User } from '../users/entities/user.entity';
import { CreatePayeeDto } from './dto/create-payee.dto';
import { UpdatePayeeDto } from './dto/update-payee.dto';
import { Payee } from './entities/payee.entity';

@Injectable()
export class PayeesService {
  constructor(@InjectRepository(Payee) private payeeRepo: Repository<Payee>) {}

  create(payee: Payee) {
    return this.payeeRepo.save(payee);
  }

  findAll(user: User) {
    return this.payeeRepo
      .createQueryBuilder('payee')
      .leftJoinAndSelect('payee.book', 'book')
      .leftJoin('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .getMany();
  }

  findAllUsingBook(book: Book) {
    return this.payeeRepo
      .createQueryBuilder('payee')
      .leftJoinAndSelect('payee.book', 'book')
      .where('book.id = :bookId', { bookId: book.id })
      .getMany();
  }

  findOne(id: Payee['id'], user: User) {
    return this.payeeRepo
      .createQueryBuilder('payee')
      .leftJoinAndSelect('payee.book', 'book')
      .leftJoinAndSelect('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .andWhere('payee.id = :id', { id })
      .getOne();
  }

  findOneUsingBook(id: Payee['id'], book: Book) {
    return this.payeeRepo
      .createQueryBuilder('payee')
      .leftJoinAndSelect('payee.book', 'book')
      .where('book.id = :bookId', { bookId: book.id })
      .andWhere('payee.id = :id', { id })
      .getOne();
  }

  update(payee: Payee) {
    return this.payeeRepo.save(payee);
  }

  remove(payee: Payee) {
    return this.payeeRepo.remove(payee);
  }
}
