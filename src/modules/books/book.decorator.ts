import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const UserBook = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.book;
  },
);
