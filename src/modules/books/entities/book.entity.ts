import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Currency } from '../../currency/entities/currency.entity';
import { BookToUser } from './book-to-user.entity';
import { Category } from '../../categories/entities/category.entity';
import { Account } from 'src/modules/accounts/entities/account.entity';
import { Payee } from 'src/modules/payees/entities/payee.entity';
import { Transaction } from 'src/modules/transactions/entities/transaction.entity';

export enum DateFormat {
  YYYYMMDD = 'yyyy/mm/dd',
}

export enum SalaryType {
  WEEKLY = 'weekly',
  // BIWEEKLY = 'bi-weekly',
  // SEMIMONTHLY = 'semi-monthly',
  MONTHLY = 'monthly',
}

@Entity()
export class Book {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column({ name: 'date_format', enum: DateFormat })
  dateFormat: DateFormat;

  @ApiProperty()
  @Column({ name: 'salary_day_chosen' })
  salaryDayChosen: string;

  @ApiProperty()
  @Column({ name: 'salary_type', enum: SalaryType })
  salaryType: string;

  @ManyToOne(
    type => Currency,
    currency => currency.books,
  )
  @JoinColumn({ name: 'currency_id' })
  currency: Currency;

  @OneToMany(
    type => BookToUser,
    bookToUser => bookToUser.book,
  )
  bookToUsers!: BookToUser[];

  @OneToMany(
    type => Category,
    category => category.book,
  )
  categories!: Category[];

  @OneToMany(
    type => Account,
    account => account.book,
  )
  accounts!: Account[];

  @OneToMany(
    type => Payee,
    payee => payee.book,
  )
  payees!: Payee[];

  @OneToMany(
    type => Transaction,
    transaction => transaction.book,
  )
  transactions!: Transaction[];
}
