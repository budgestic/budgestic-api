import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Book } from './book.entity';
import { User } from '../../users/entities/user.entity';

@Entity('book__user')
export class BookToUser {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ type: 'boolean', name: 'is_owner' })
  isOwner: boolean;

  @ManyToOne(
    type => Book,
    book => book.bookToUsers,
  )
  @JoinColumn({ name: 'book_id' })
  public book!: Book;

  @ManyToOne(
    type => User,
    user => user.bookToUsers,
  )
  @JoinColumn({ name: 'user_id' })
  public user!: User;
}
