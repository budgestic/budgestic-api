import { Module } from '@nestjs/common';
import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { CurrencyModule } from '../currency/currency.module';
import { Book } from './entities/book.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookToUser } from './entities/book-to-user.entity';

@Module({
  controllers: [BooksController],
  providers: [BooksService],
  imports: [TypeOrmModule.forFeature([Book, BookToUser]), CurrencyModule],
  exports: [BooksService],
})
export class BooksModule {}
