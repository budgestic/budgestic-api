import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  UsePipes,
  ValidationPipe,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { CurrencyService } from '../currency/currency.service';
import { AuthUser } from '../users/authUser.decorator';
import { User } from '../users/entities/user.entity';
import { BooksService } from './books.service';
import { CreateBookDto } from './dto/create-book.dto';
import { Book } from './entities/book.entity';

@UseGuards(JwtAuthGuard)
@UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
@Controller('books')
export class BooksController {
  constructor(
    private readonly booksService: BooksService,
    private readonly currencyService: CurrencyService,
  ) {}

  @Post()
  async create(@Body() body: CreateBookDto, @AuthUser() user: User) {
    const currency = await this.currencyService.findOne(body.currencyId);
    if (!currency) {
      throw new BadRequestException('Invalid currency');
    }
    let book = new Book();
    book.dateFormat = body.dateFormat;
    book.name = body.name;
    book.salaryDayChosen = body.salaryDayChosen;
    book.salaryType = body.salaryType;
    book.currency = currency;
    book = await this.booksService.create(book, user);
    return { book };
  }

  @Get()
  async findAll(@AuthUser() user: User) {
    const books = await this.booksService.findAll(user);
    return { books };
  }

  @Get(':id')
  async findOne(@Param('id') id: number, @AuthUser() user: User) {
    const book = await this.booksService.findOne(id, user);
    if (!book) throw new NotFoundException('Book not found!');
    return { book };
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() body: CreateBookDto,
    @AuthUser() user: User,
  ) {
    const book = await this.booksService.findOne(id, user);
    if (!book) throw new NotFoundException('Book not found!');

    if (body.currencyId !== book.currency.id) {
      const currency = await this.currencyService.findOne(body.currencyId);
      if (!currency) throw new BadRequestException('Invalid currency');
      book.currency = currency;
    }

    book.dateFormat = body.dateFormat;
    book.name = body.name;
    book.salaryDayChosen = body.salaryDayChosen;
    book.salaryType = body.salaryType;

    const result = await this.booksService.update(book);
    return { book: result };
  }

  @Delete(':id')
  async remove(@Param('id') id: number, @AuthUser() user: User) {
    const book = await this.booksService.findOne(id, user);
    if (!book) throw new NotFoundException('Book not found!');
    await this.booksService.remove(book);
    return 'Book removed!';
  }
}
