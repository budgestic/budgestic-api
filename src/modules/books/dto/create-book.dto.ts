import { IsEnum, IsInt, IsNotEmpty, IsString } from 'class-validator';
import { DateFormat, SalaryType } from '../entities/book.entity';

export class CreateBookDto {
  @IsNotEmpty()
  currencyId: number;

  @IsNotEmpty()
  name: string;

  @IsEnum(DateFormat)
  dateFormat: DateFormat;

  @IsNotEmpty()
  salaryDayChosen: string;

  @IsEnum(SalaryType)
  salaryType: SalaryType;
}
