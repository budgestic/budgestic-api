import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { BookToUser } from './entities/book-to-user.entity';
import { Book } from './entities/book.entity';

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(Book) private bookRepo: Repository<Book>,
    private connection: Connection,
  ) {}

  create = async (book: Book, user: User): Promise<Book> => {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const newBook = await queryRunner.manager.save(book);
      const bookToUser = new BookToUser();
      bookToUser.isOwner = true;
      bookToUser.book = newBook;
      bookToUser.user = user;
      await queryRunner.manager.save(bookToUser);
      await queryRunner.commitTransaction();
      return newBook;
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  };

  async findAll(user: User): Promise<Book[]> {
    return this.bookRepo
      .createQueryBuilder('book')
      .leftJoin('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .leftJoinAndSelect('book.currency', 'currency')
      .where('user.id = :userId', { userId: user.id })
      .getMany();
  }

  async findOne(id: number, user: User): Promise<Book> {
    return this.bookRepo
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.currency', 'currency')
      .leftJoinAndSelect('book.bookToUsers', 'bookToUsers')
      .leftJoin('bookToUsers.user', 'user')
      .where('book.id = :id', { id })
      .andWhere('user.id = :userId', { userId: user.id })
      .getOne();
  }

  async update(book: Book): Promise<Book> {
    return this.bookRepo.save(book);
  }

  remove = async (book: Book) => {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      await queryRunner.manager.remove(book.bookToUsers);
      await queryRunner.manager.remove(book);
      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  };
}
