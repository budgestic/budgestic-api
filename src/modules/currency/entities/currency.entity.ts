import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Book } from '../../books/entities/book.entity';

@Entity()
export class Currency {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column({ type: 'char', length: 3 })
  code: string;

  @ApiProperty()
  @Column({ name: 'unicode_hex' })
  unicodeHex: string;

  @OneToMany(
    type => Book,
    book => book.currency,
  )
  books: Book[];
}
