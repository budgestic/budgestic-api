import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Currency } from './entities/currency.entity';

@Injectable()
export class CurrencyService {
  constructor(
    @InjectRepository(Currency) private currencyRepo: Repository<Currency>,
  ) {}

  async findAll() {
    return this.currencyRepo.find();
  }

  async findOne(id: number) {
    return this.currencyRepo.findOne(id);
  }
}
