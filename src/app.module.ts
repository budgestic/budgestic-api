import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthController } from './modules/auth/auth.controller';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { UsersController } from './modules/users/users.controller';
import { EmailModule } from './providers/email/email.module';
import { RegisteredDevicesModule } from './modules/registered-devices/registered-devices.module';
import { BooksModule } from './modules/books/books.module';
import { CurrencyModule } from './modules/currency/currency.module';
import { CategoriesModule } from './modules/categories/categories.module';
import { SubcategoriesModule } from './modules/subcategories/subcategories.module';
import { AccountsModule } from './modules/accounts/accounts.module';
import { PayeesModule } from './modules/payees/payees.module';
import { TransactionsModule } from './modules/transactions/transactions.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot(),
    AuthModule,
    UsersModule,
    EmailModule,
    RegisteredDevicesModule,
    BooksModule,
    CurrencyModule,
    CategoriesModule,
    SubcategoriesModule,
    AccountsModule,
    PayeesModule,
    TransactionsModule,
  ],
  controllers: [AppController, AuthController, UsersController],
  providers: [AppService],
})
export class AppModule {}
