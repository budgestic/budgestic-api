import {
  BadRequestException,
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
  NotFoundException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { BooksService } from '../modules/books/books.service';

@Injectable()
export class BookInterceptor implements NestInterceptor {
  constructor(private readonly booksService: BooksService) {}

  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const { user } = request;
    const bookId =
      +request.params.bookId || +request.body.bookId || +request.query.bookId;

    if (!!!bookId) throw new BadRequestException('Book ID is invalid');

    const book = await this.booksService.findOne(bookId, user);
    if (!book) throw new NotFoundException('Book not found');

    request.book = book;
    return next.handle();
  }
}
