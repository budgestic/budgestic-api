import { BookInterceptor } from './book.interceptor';

describe('BookInterceptor', () => {
  it('should be defined', () => {
    expect(new BookInterceptor()).toBeDefined();
  });
});
