import {
  BadRequestException,
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
  NotFoundException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { CategoriesService } from 'src/modules/categories/categories.service';

@Injectable()
export class CategoryInterceptor implements NestInterceptor {
  constructor(private readonly categoriesService: CategoriesService) {}

  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const { user } = request;
    const categoryId =
      +request.params.categoryId ||
      +request.body.categoryId ||
      +request.query.categoryId;

    if (!!!categoryId) throw new BadRequestException('Category ID is invalid');

    const category = await this.categoriesService.findOne(categoryId, user);
    if (!category) throw new NotFoundException('Category not found');

    request.category = category;
    return next.handle();
  }
}
